# Shopify Validator

This is a simple, dynamic validator created by Robert Lake to showcase as part of the Shopify Winter 2018 application process.


## Notes

The app was written to be dynamic, importable, platform agnostic and easily extensible.

## Requirements

This app can be easily run with Docker.

```docker run rlake80s/shopify_validator```

Alternately, it can be run without Docker.

Doing so requires a Python 3.4+ installation (I used 3.5.3), and a single external library.

That library can be installed by running the following at the command prompt from within the main project directorry.

```$ pip install -r requirements.txt```

After that, just ```python run.py``` from command prompt and you're done.
