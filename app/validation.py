import json
import math
import requests
from app.tools import name_validation, email_validation, age_validation, newsletter_validation



# The main validation function
def data_validation(request_url):

    # invalid customer object initialization
    invalid_customers = {"invalid_customers": []}

    # initial endpoint query
    r = requests.get(
        request_url)
    result = json.loads(
        r.text)

    # determine the number of pages to cycle through at this endpoint
    customers_per_page = result["pagination"]["per_page"]
    total_customers = result["pagination"]["total"]
    total_iterations = math.ceil(
        total_customers / customers_per_page)

    # create our validations object that we can iterate through for each customer
    validations = result["validations"]

    # start iterating through the pages
    for page_index in range(total_iterations):

        # get the page object
        r = requests.get(
            request_url + "?page=" + str(page_index + 1))
        result = json.loads(
            r.text)

        # capture the customers from the page into an object
        customers = result["customers"]

        # start iterating through the customers on the current page
        for customer in customers:

            # initialize the list that we'll use to store validation infractions for this customer
            invalid_fields = []

            # iterate through the validations present at this endpoint and call each validator tool as required
            for objects in validations:
                for key, values in objects.items():
                    if key == "name":
                        for inner_key, inner_values in values.items():
                            validation = name_validation(customer, invalid_fields, inner_key, inner_values)
                    if key == "email":
                        for inner_key, inner_values in values.items():
                            validation = email_validation(customer, invalid_fields, inner_key, inner_values)
                    if key == "age":
                        for inner_key, inner_values in values.items():
                            validation = age_validation(customer, invalid_fields, inner_key, inner_values)
                    if key == "newsletter":
                        for inner_key, inner_values in values.items():
                            validation = newsletter_validation(customer, invalid_fields, inner_key, inner_values)

                    # if the validation var we've used for running our validations has a value, add it to the invalid_fields list
                    if validation != None:
                        invalid_fields.append(validation)

            # convert our list of infractions into a set and then back into a list to remove any duplicate values
            # ie. if there are multiple infractions of the same type
            unique_invalids = list(set(invalid_fields))

            # if the customer's input has any infractions, add them to the invalid_customers object
            if len(unique_invalids) > 0:
                invalid_dict = {
                    "id": customer["id"], "invalid_fields": unique_invalids}
                invalid_customers["invalid_customers"].append(
                    invalid_dict)

    # convert the python dictionary object into valid JSON and return it to the caller
    return json.dumps(
        invalid_customers, indent=2, sort_keys=True)
