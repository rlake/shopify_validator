# Validator tools to be used as part of the main validation.py script


# dictionary for converting string type values to actual python types
type_dict = {
    "boolean": bool,
    "string": str,
    "number": int
}

# Name validation function
def name_validation(customer, invalid_fields, key, value):

    if key == "required":
        if value == True:
            try:
                assert customer["name"] != None
            except:
                return "name"

    if key == "type":
        try:
            assert type(customer["name"]) == type_dict[value]
        except:
            return "name"

    if key == "length":
        if "min" in value:
            try:
                assert len(customer["name"]) >= value["min"]
            except:
                return "name"
        if "max" in value:
            try:
                assert len(customer["name"]) <= value["max"]
            except:
                return "name"


# email validation function
def email_validation(customer, invalid_fields, key, value):

    if key == "required":
        if value == True:
            try:
                assert customer["email"] != None
            except:
                return "email"

# age validation function
def age_validation(customer, invalid_fields, key, value):

    if key == "required":
        if value == True:
            try:
                assert customer["age"] != None
            except:
                return "age"

    if key == "type":
        try:
            assert type(customer["age"]) == type_dict[value]
        except:
            return "age"

# newsletter validation function
def newsletter_validation(customer, invalid_fields, key, value):

    if key == "required":

        if value == True:
            try:
                assert customer["newsletter"] != None
            except:
                return "newsletter"

    if key == "type":
        try:
            assert type(customer["newsletter"]) == type_dict[value]
        except:
            return "newsletter"
