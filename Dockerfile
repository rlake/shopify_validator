# Use an official Python runtime as a parent image
FROM python:3.5.3-slim

# Set the working directory to /app
WORKDIR /shopify_validator

# Copy the current directory contents into the container at /app
ADD . /shopify_validator

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Run app.py when the container launches
CMD ["python", "run.py"]
